#!/bin/sh

RELEASEDATE=20130203
SVNREV=4711

svn co -r ${SVNREV} \
	https://sauerbraten.svn.sourceforge.net/svnroot/sauerbraten \
	sauerbraten-0.0.${RELEASEDATE}

find sauerbraten-0.0.${RELEASEDATE} -type d -name '.svn' \
	-exec rm -rf "{}" \;
rm -rf sauerbraten-0.0.${RELEASEDATE}/vcpp
rm -rf sauerbraten-0.0.${RELEASEDATE}/xcode
rm -rf sauerbraten-0.0.${RELEASEDATE}/lib
rm -rf sauerbraten-0.0.${RELEASEDATE}/bin
rm -rf sauerbraten-0.0.${RELEASEDATE}/bin_unix
rm -f sauerbraten-0.0.${RELEASEDATE}/sauerbraten.bat
rm -f sauerbraten-0.0.${RELEASEDATE}/sauerbraten.pdb
rm -f sauerbraten-0.0.${RELEASEDATE}/sauerbraten_unix
rm -f sauerbraten-0.0.${RELEASEDATE}/server.bat
rm -f sauerbraten-0.0.${RELEASEDATE}/server-init.cfg
rm -rf sauerbraten-0.0.${RELEASEDATE}/src

tar -czf sauerbraten_0.0.${RELEASEDATE}.orig.tar.gz \
	sauerbraten-0.0.${RELEASEDATE}

rm -rf sauerbraten-0.0.${RELEASEDATE}
