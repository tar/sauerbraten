sauerbraten (0.0.20140302-2) unstable; urgency=medium

  * Update my e-mail address.
  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.1.4.
  * Drop deprecated menu file and xpm icon.
  * Update VCS-fields. Move to salsa.debian.org.

 -- Markus Koschany <apo@debian.org>  Sat, 16 Jun 2018 23:59:52 +0200

sauerbraten (0.0.20140302-1) unstable; urgency=medium

  * Team upload.
  * Rename sauerbraten-data source and binary packages to sauerbraten. From now
    on sauerbraten is a content package and a game that depends on the free
    cube2 engine.
  * Bump compat level to 9 and require debhelper >= 9.
  * debian/control:
    - sauerbraten is now an arch:all binary package that installs the same
      content as the former sauerbraten-data package.
    - Drop sauerbraten-dbg. Debug symbols are provided by cube2-dbg.
    - sauerbraten-server: dependency package for pulling in cube2-server.
    - Add Breaks, Replaces and Conflicts fields for the transition.
    - Do not recommend sauerbraten-wake6 anymore. Wake6 is now part of
      cube2-data.
    - Add myself to Uploaders.
  * Add man pages, desktop and menu files and icons to the new content package.
  * Use new high resolution icon for desktop and menu files. (Closes: #649681)
  * Update sauerbraten and sauerbraten-server wrappers due to the new cube2
    engine.
  * Add clean file.
  * Build with --parallel.
  * Update debian/copyright. Add missing licenses. Use a more permissive
    license for the Debian packaging.

 -- Markus Koschany <apo@gambaru.de>  Tue, 01 Apr 2014 14:52:02 +0200

sauerbraten-data (0.0.20130203-1) unstable; urgency=medium

  * Team upload.
  * Add myself to uploaders.
  * New upstream release.
  * Update Standards version to 3.9.5, no changes required.

 -- Vincent Cheng <vcheng@debian.org>  Tue, 21 Jan 2014 17:26:41 -0800

sauerbraten-data (0.0.20100728+repack-1) unstable; urgency=low

  * Team upload.
  * Repack .orig.tar.gz from an earlier snapshot of upstream's Subversion
    repository. Checksums of maps should now match the checksums of the maps in
    the official upstream tarball. (Closes: #609084)

 -- Bruno "Fuddl" Kleinert <fuddl@debian.org>  Fri, 24 Jun 2011 18:57:39 +0200

sauerbraten-data (0.0.20100728-2) unstable; urgency=low

  * Fix typos in the package description
  * Use debhelper 8 to build the package
    * Build-depend on debhelper (>= 8)
    * Use the debhelper 8 minimal debian/rules file
  * Update to standards version 3.9.2. No changes

 -- Bruno "Fuddl" Kleinert <fuddl@debian.org>  Sat, 04 Jun 2011 01:14:00 +0200

sauerbraten-data (0.0.20100728-1) unstable; urgency=low

  * New upstream release
  * Update copyright information according to the content of the new release
  * Migrate to source format 3.0 (quilt)
  * Fix a lintian warning about missing ${misc:Depends} in the Depends field
  * Update to latest standards version 3.9.1 without changes

 -- Bruno "Fuddl" Kleinert <fuddl@debian.org>  Fri, 30 Jul 2010 11:38:47 -0400

sauerbraten-data (0.0.20090504-1) unstable; urgency=low

  * New Upstream Version
  * Add new license and copyright snippets to debian/copyright
  * Call dh_prep instead of dh_clean -k in debian/rules
  * Update Vcs-* fields to reflect new repository location

 -- Bruno "Fuddl" Kleinert <fuddl@debian.org>  Thu, 02 Jul 2009 17:33:18 +0200

sauerbraten-data (0.0.20080620-1) unstable; urgency=low

  * New upstream release
  * Update debian/copyright
  * Update debian/rules to remove the executable bit from some more plain text
    files

 -- Bruno "Fuddl" Kleinert <fuddl@tauware.de>  Sat, 28 Jun 2008 10:58:08 +0200

sauerbraten-data (0.0.20071227-2) unstable; urgency=low

  * Fix malformed Conflicts-field in control file (Closes: #467449)

 -- Bruno "Fuddl" Kleinert <fuddl@tauware.de>  Sun, 23 Mar 2008 20:15:58 +0100

sauerbraten-data (0.0.20071227-1) unstable; urgency=low

  [ Bruno "Fuddl" Kleinert ]
  * New upstream release
  * Update to Standards version 3.7.3.0. No changes necessary
  * Update my email address
  * Conflict against out-of-date engine package (Closes: #440246)

 -- Bruno "Fuddl" Kleinert <fuddl@tauware.de>  Mon, 29 Jan 2008 22:09:30 +0100

sauerbraten-data (0.0.20070819-1) unstable; urgency=low

  * New upstream release
  * Updated address of the FSF in the copyright file

 -- Bruno "Fuddl" Kleinert <fuddl@gmx.de>  Sun, 26 Aug 2007 18:25:04 +0200

sauerbraten-data (0.0.20070413-1) unstable; urgency=low

  * New upstream release

 -- Bruno "Fuddl" Kleinert <fuddl@gmx.de>  Sat, 14 Apr 2007 22:56:01 +0200

sauerbraten-data (0.0.20061204-1) unstable; urgency=low

  * Initial release

 -- Bruno "Fuddl" Kleinert <fuddl@gmx.de>  Tue,  9 Jan 2007 18:19:30 +0100
